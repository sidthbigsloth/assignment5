<?php

class Club
{
	public $id;
	public $name;
	public $city;
	public $county;

	public function __construct($id, $name, $city, $county)  
    {  
        $this->id = $id;
        $this->name = $name;
	    $this->city = $city;
	    $this->county = $county;
    } 
}

class Skier
{
	public $username;
	public $firstname;
	public $lastname;
	public $yearofbirth;

	public function __construct($username, $firstname, $lastname, $yearofbirth)  
    {  
        $this->username = $username;
        $this->firstname = $firstname;
	    $this->lastname = $lastname;
	    $this->yearofbirth = $yearofbirth;
    } 
}

class Affiliation 
{
	public $id;
	public $username;
	public $fallyear;

	public function __construct($id, $username, $fallyear)
	{
		$this->id = $id;
		$this->username = $username;
		$this->fallyear = $fallyear;
	}
}

class Log 
{
	public $username;
	public $date;
	public $area;
	public $distance;

	public function __construct($username, $date, $area, $distance)
	{
		$this->username = $username;
		$this->date = $date;
		$this->area = $area;
		$this->distance = $distance;
	}
}

class Distance
{
	public $username;
	public $fallyear;
	public $distance;

	public function __construct($username, $fallyear, $distance)
	{
		$this->username = $username;
		$this->fallyear = $fallyear;
		$this->distance = $distance;
	}
}

?>